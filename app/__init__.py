from registration.signals import user_activated

from app.models import User, UserProfile

def user_activated_hdlr(sender, user, request):
    profile = UserProfile.objects.get_or_create(
        user=user
    )