# coding: utf-8
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfile(models.Model):
    photo = models.ImageField(upload_to='upload/', verbose_name=u'Аватар', blank=True)
    city = models.CharField(max_length=100, verbose_name=u'Город', blank=True)
    sex = models.BooleanField(verbose_name=u'Мужской пол', default=True)
    user = models.OneToOneField(User, verbose_name=u'Пользователь', related_name='profile')

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = u'Профиль пользователя'
        verbose_name_plural = u'Профили пользователей'

class ExtraCompanyData(models.Model):
    user = models.OneToOneField(User)
    title = models.CharField(max_length=1024, verbose_name=u'Название компании')
    inn = models.IntegerField(verbose_name=u'ИНН')
    phone = models.CharField(max_length=32, verbose_name=u'Телефон')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Дополнительные данные компании'
        verbose_name_plural = u'Дополнительные данные компании'

class Company(models.Model):
    menu = models.TextField(verbose_name=u'Меню')
    title = models.CharField(verbose_name=u'Название', max_length=100)
    description = models.CharField(max_length=1024, verbose_name=u'Описание')
    logo = models.ImageField(upload_to='uploads/', verbose_name=u'Логотип')
    text = models.TextField(verbose_name=u'Текст о компании')
    link = models.CharField(verbose_name=u'Ссылка на сайт', blank=True, max_length=100)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Компания'
        verbose_name_plural = u'Компании'


class Shop(models.Model):
    company = models.ForeignKey(Company, verbose_name=u'Компания', related_name='shops')
    address = models.CharField(verbose_name=u'Адрес', max_length=1024)
    phone = models.CharField(max_length=16, verbose_name=u'Телефон')
    workhours = models.TextField(verbose_name=u'Часы работы')
    wifi = models.BooleanField(verbose_name=u'Наличие Wi Fi', default=False)
    lng = models.FloatField(verbose_name=u'Долгота')
    lat = models.FloatField(verbose_name=u'Широта')

    class Meta:
        verbose_name = u'Магазин'
        verbose_name_plural = u'Магазины'

    def __unicode__(self):
        return u'[{}]: {}'.format(self.company.title, self.address)

class Feedback(models.Model):
    company = models.ForeignKey(Company, verbose_name=u'Компания', related_name='feedbacks')
    name = models.CharField(max_length=100, verbose_name=u'Имя')
    photo = models.ImageField(upload_to='uploads/', verbose_name=u'Фото', null=True, blank=True)
    text = models.TextField(verbose_name=u'Текст')
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')
    user = models.ForeignKey(User, verbose_name=u'Пользователь', null=True, related_name='feedbacks')

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'

    def __unicode__(self):
        return u'{} [{}]: {}'.format(self.created, self.company.title, self.name)

class ActionGroup(models.Model):
    name = models.CharField(verbose_name=u'Название группы', max_length=100)
    icon = models.ImageField(upload_to='uploads/', verbose_name=u'Значок')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Группа акций'
        verbose_name_plural = u'Группы акций'

class Action(models.Model):
    company = models.ForeignKey(Company, verbose_name=u'Компания', related_name='actions')
    title = models.CharField(max_length=1024, verbose_name=u'Название акции')
    banner = models.ImageField(upload_to='uploads/', verbose_name=u'Баннер для акции')
    text = models.TextField(verbose_name=u'Текст акции')
    premium = models.BooleanField(default=False, verbose_name=u'Премиум акция')
    group = models.ForeignKey(ActionGroup, verbose_name=u'Группа акций', related_name='actions')
    users_likes = models.ManyToManyField(User,
        verbose_name=u'Пользователи, поставившие лайки',
        related_name='actions_likes')
    users_dislikes = models.ManyToManyField(User,
        verbose_name=u'Пользователи, поставившие дизлайки',
        related_name='actions_dislikes')
    users_delayed = models.ManyToManyField(User,
        verbose_name=u'Пользователи, отложившие акцию в закладки',
        related_name='actions_delayed')

    class Meta:
        verbose_name = u'Акция'
        verbose_name_plural = u'Акции'

    def __unicode__(self):
        return u'[{}]: {}'.format(self.company.title, self.title)