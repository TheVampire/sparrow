from django import forms
from registration.forms import RegistrationFormUniqueEmail

class RegistrationCompanyForm(RegistrationFormUniqueEmail):
    inn = forms.IntegerField()
    title = forms.CharField(max_length=1024)
    phone = forms.CharField(max_length=32)