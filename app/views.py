#coding: utf-8
from django.views.generic import ListView, DetailView, TemplateView, View
from app.models import Action, Company, ActionGroup, UserProfile

from app.forms import RegistrationCompanyForm
from registration.views import RegistrationView
from app.models import ExtraCompanyData
from django.contrib.auth import authenticate
from registration import signals
from django.contrib.auth.models import User
from django.contrib.auth import views as auth_views
from django.contrib.auth import logout
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseForbidden
from django.conf import settings
import os
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

@csrf_exempt
@require_POST
def upload( request ):

    # The assumption here is that jQuery File Upload
    # has been configured to send files one at a time.
    # If multiple files can be uploaded simulatenously,
    # 'file' may be a list of files.
    img_temp = NamedTemporaryFile(delete=True)
    for chunk in request.FILES['file'].chunks():
        img_temp.write(chunk)
    img_temp.flush()

    request.user.profile.photo.save(request.FILES['file'].name, File(img_temp))

    instance = request.user.profile

    basename = os.path.basename( instance.photo.path )

    file_dict = {
        'url': os.path.join(settings.MEDIA_URL, 'upload', basename),

        }

    return HttpResponse(file_dict['url'])

class DeleteAccountView(View):
    def get(self, request, *args, **kwargs):
        rt = '0'
        if not request.user.is_superuser:
            logout(request)
            request.user.is_active = False
            rt = '1'
        return HttpResponse(rt)


class BookmarksListView(ListView):
    template_name = 'client_bookmarks.html'
    model = Action
    context_object_name = 'actions'

    def get_queryset(self):
        return self.request.user.actions_delayed.all()

    def get_context_data(self, **kwargs):
        ctx = super(BookmarksListView, self).get_context_data(**kwargs)
        groups = ActionGroup.objects.all()
        groups_list = []
        for g in groups:
            cnt = self.request.user.actions_delayed.filter(group=g).count()
            groups_list.append((g, cnt))
        ctx['bookmarks_groups'] = groups_list
        return ctx

class BookmarksAddView(View):

    def get(self, request, *args, **kwargs):
        action_pk = kwargs.get('pk')
        action = Action.objects.get(pk=action_pk)
        action.users_delayed.add(request.user)
        return HttpResponse(action.users_delayed.count())

class ActionLikeView(View):

    def get(self, request, *args, **kwargs):
        action_pk = kwargs.get('pk')
        action = Action.objects.get(pk=action_pk)
        action.users_likes.add(request.user)
        return HttpResponse(action.users_likes.count())

class ActionDislikeView(View):

    def get(self, request, *args, **kwargs):
        action_pk = kwargs.get('pk')
        action = Action.objects.get(pk=action_pk)
        action.users_dislikes.add(request.user)
        return HttpResponse(action.users_dislikes.count())

class ProfileView(TemplateView):
    template_name = 'client_profile.html'

    def get_context_data(self, **kwargs):
        ctx = super(ProfileView, self).get_context_data(**kwargs)
        ctx['recals'] = self.request.user.feedbacks.order_by('-created')
        breadcrumbs = (
            (reverse('home'), u'Главная'),
            (reverse('profile'), u'Личный кабинет')
        )
        ctx['breadcrumbs'] = breadcrumbs
        return ctx

class RegistrationCompanyView(RegistrationView):
    template_name = 'registration/registration_company_form.html'
    form_class = RegistrationCompanyForm
    success_url = 'registration_complete'

    def register(self, request, **kwargs):
        username,email, password = kwargs['username'],kwargs['email'],kwargs['password1']
        inn,title,phone = kwargs['inn'],kwargs['title'],kwargs['phone']

        User.objects.create_user(username,email,password)
        new_user = authenticate(username=username, password=password)
        ExtraCompanyData.objects.create(
            inn=inn,
            title=title,
            phone=phone,
            user=new_user
        )

        signals.user_registered.send(sender=self.__class__, user=new_user, request=request)

        return new_user


class HomeView(ListView):
    model = Action
    context_object_name = 'actions'
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        ctx = super(HomeView, self).get_context_data(**kwargs)
        ctx['premium'] = Action.objects.filter(premium=True)
        ctx['active'] = "home"
        return ctx

class ActionsListView(ListView):
    model = Action
    context_object_name = 'actions'
    template_name = 'actions.html'

    def get_context_data(self, **kwargs):
        ctx = super(ActionsListView, self).get_context_data(**kwargs)
        ctx['active'] = "actions"
        return ctx

class CompanyDetailView(DetailView):
    model = Company
    context_object_name = 'company'
    template_name = 'detail_company.html'

def login(request, *args, **kwargs):
    if request.method == 'POST':
        request.session.set_test_cookie()
        login_form = AuthenticationForm(request, request.POST)
        if login_form.is_valid():
            if not request.POST.get('remember_me', None):
                request.session.set_expiry(0)
            if request.is_ajax:
                user = auth_views.login(request, login_form.get_user())
                if user is not None:
                    return HttpResponse(request.REQUEST.get('next', '/'))
    return HttpResponseForbidden()