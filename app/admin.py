from django.contrib import admin

from app.models import Action, Feedback, Shop, Company, ExtraCompanyData, ActionGroup, UserProfile

admin.site.register(Action)
admin.site.register(Feedback)
admin.site.register(Shop)
admin.site.register(Company)
admin.site.register(ExtraCompanyData)
admin.site.register(ActionGroup)
admin.site.register(UserProfile)