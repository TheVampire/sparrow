from django.conf.urls import patterns, include, url
from app.views import HomeView, ActionsListView, CompanyDetailView, RegistrationCompanyView, ProfileView, \
ActionLikeView, ActionDislikeView, BookmarksAddView, BookmarksListView, DeleteAccountView, upload

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^actions/$', ActionsListView.as_view(), name='actions'),
    url(r'^action/like/(?P<pk>\d+)/$', ActionLikeView.as_view(), name='action_like'),
    url(r'^action/dislike/(?P<pk>\d+)/$', ActionDislikeView.as_view(), name='action_dislike'),
    url(r'^action/bookmarks/add/(?P<pk>\d+)/$', BookmarksAddView.as_view(), name='bookmarks_add'),
    url(r'^company/(?P<pk>\d+)/$', CompanyDetailView.as_view(), name='company'),
    url(r'^company/registration/$', RegistrationCompanyView.as_view(), name='company_registration'),

    url(r'^upload/$', upload, name='upload'),
    url(r'^profile/$', ProfileView.as_view(), name='profile'),
    url(r'^profile/bookmarks/$', BookmarksListView.as_view(), name='bookmarks'),

    # url(r'^sparrow/', include('sparrow.foo.urls')),
    url(r'^accounts/delete/$', DeleteAccountView.as_view(), name='accounts_delete'),
    url(r'^accounts/login/','app.views.login', name='auth_login'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
#    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # for social auth
    url('', include('social.apps.django_app.urls', namespace='social'))
)
